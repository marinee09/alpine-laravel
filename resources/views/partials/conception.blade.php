<!--Section Conception -->
<section id="conception">
    <div class="container">
        <div class="row pt-5">
            <div class="col-12">
                <h1 class="text-center text-blue writing">Conception</h1>
                <p class="text-justify lead">Pour une voiture sportive, le poids est un élément essentiel. La
                    répartition
                    des masses optimale de l’A110, ses dimensions compactes et la légèreté de sa carrosserie en
                    aluminium lui confèrent plaisir de conduite et agilité.
                </p>
            </div>
            <div class="col-sm-4 pt-5">
                <h3 class="text-blue">Robustesse et légèreté
                </h3>
                <p class="lead text-justify">L’A110 tire de sa structure en aluminium robustesse, extrême agilité et
                    maniabilité. Le design épuré et les éléments de carrosserie rivetés et soudés font de nouveau
                    gagner en légèreté et en robustesse.</p>
            </div>
            <div class="col-sm-8">
                <!-- Slider -->
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ asset('assets/sources-homepage/conception/alpine-bone.jpg') }}" class="img-fluid" alt="Slide-01">
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('assets/sources-homepage/conception/alpine-skin.jpg') }}" class="img-fluid" alt="Slide-02">
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('assets/sources-homepage/conception/visuel_legerete_2_desktop1.jpg') }}" class="img-fluid"
                                alt="Slide-03">
                        </div>
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>
                    <!-- Fin slider -->
                </div>
            </div>
        </div>
</section>
    <!--end of section-->
