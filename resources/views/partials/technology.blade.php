<!--Section Technologie-->
<section id="technology">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center pt-5 text-blue writing">Technologie</h1>
                <p class="lead text-justify pt-2">La nouvelle A110 couple les singularités propres à Alpine aux
                    dernières technologies. La boîte de vitesse automatique et le choix entre trois modes de conduite
                    permettent une meilleure appropriation du véhicule.</p>
                </p>
                <hr>
            </div>
            <div class="col-sm-12">
                <h3 class="text-blue text-center">Suspension et freins</h3>
                <p class="text-justify pt-2">En plus de sa structure légère, l’A110 tient son agilité et sa précision
                    de pilotage de son système de suspension à double triangulation. Un choix qui lui confère meilleure
                    tenue de route et stabilité sur des surfaces irrégulières. Les freins de l'équipementier Brembo
                    offrent ce qui se fait de mieux en termes d’endurance et de distance de freinage, que ce soit sur
                    circuit ou dans les lacets d’une route de montagne.</p>
            </div>
            <div class="col-sm-6 embed-responsive embed-responsive-16by9">
                <video autoplay loop width='550'>
                    <source src="{{ asset('assets/sources-homepage/technologie/Center-of-gravity-FR_LOW.mov') }}" class="img-fluid"
                            alt="Slide-01"></video>
            </div>
            <div class="col-sm-6">
                <!-- Slider -->
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <li data-target="#demo" data-slide-to="0" class="active"></li>
                        <li data-target="#demo" data-slide-to="1"></li>
                        <li data-target="#demo" data-slide-to="2"></li>
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="{{ asset('assets/sources-homepage/technologie/Technical-front-wheel-A110_mobile.jpg') }}" class="img-fluid"
                            alt="Slide-02">
                        </div>
                        <div class="carousel-item">
                            <img src="{{ asset('assets/sources-homepage/technologie/Technical-rear-wheel-A110_mobile.jpg')}}" class="img-fluid"
                                alt="Slide-03">
                        </div>
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                    </div>
                    <!-- Fin slider -->
                </div>
            </div>
        </div>
    </div>
</section>
<!--end of section-->