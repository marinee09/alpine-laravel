<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ConfiguratorController extends Controller
{
    public function index() {
        return view('configurator.index');
    }

    public function pure() {
        return view('configurator.pure');
    }

    public function legende() {
        return view('configurator.legende');
    }

}
